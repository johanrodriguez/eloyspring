# README #

Spring 4 Eloy Project.

### Topics to cover ###

* Web development overview
>+ Distributed applications (client, server https://docs.oracle.com/javaee/7/tutorial/overview003.htm#BNAAY)
>+ Presentation Oriented (Front End)
>+ Service Oriented (Rest, Webservices)
>+ Web Applications (https://docs.oracle.com/javaee/7/tutorial/webapp001.htm#GEYSJ)
>+ What is a Servlet? (https://docs.oracle.com/javaee/7/tutorial/servlets001.htm#BNAFE)
>+ What is a JSP? (http://docs.oracle.com/javaee/5/tutorial/doc/bnagy.html)
>+ What is JSTL? (https://docs.oracle.com/javaee/7/tutorial/overview007.htm#BNACO)
>+ WebServices: JAX-WS vs JAX-RS (https://docs.oracle.com/javaee/7/tutorial/webservices-intro003.htm#GJBJI)
>+ What is RESTful? (http://docs.oracle.com/javaee/7/tutorial/jaxrs001.htm#GIJQY)

* Apache Tomcat
>+ Java EE HTTP Server 
>+ Catalina, Coyote, Jasper, Cluster
	
* Maven 
>+ What is Maven? (https://maven.apache.org/index.html)
>+ What is a POM?

* Spring
>+ What is it? (http://docs.spring.io/spring/docs/current/spring-framework-reference/html/overview.html)
>+ Projects (http://spring.io/docs/reference)
* Spring MVC (http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html)
* Apache tiles (https://tiles.apache.org/)
* Spring i18n
* JSTL (Applied)
* Spring Data (http://docs.spring.io/spring-data/jpa/docs/current/reference/html/)
* Spring Security (http://docs.spring.io/spring-security/site/docs/3.0.x/reference/introduction.html#what-is-acegi-security)
* Rest with spring (Applied)
* Spring Mobile (http://projects.spring.io/spring-mobile/)

### How do I get set up? ###

* Pre-requisites: 
>+ Sprint Tools Suite (Download here: https://spring.io/tools)
>+ Git  (Download here: https://git-scm.com/downloads)
>+ Tortoise Git  (Download here: https://tortoisegit.org/)
>+ Apache Tomcat (Download here: https://tomcat.apache.org/download-80.cgi)
>+ Java JDK 8 (Download here: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

* Install Mongo DB
>+ Follow instructions here -> (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)
>+ Start mongo (mongod)

* Get the code (Tortoise Git)
>+ Look for the path where you want to download the code
>+ Right click, Git Clone and use the path https://bitbucket.org/johanrodriguez/eloyspring
>+ That will download all the source code for the sample

* Configure the project (Open Spring Tool Suite)
>+ Configure Tomcat: Window, Preferences, Server, Runtime Environments, Add
>+ Select Apache Tomcat v8.0 (or 7.0), Browse and look for the path where tomcat was installed, Finish
>+ Configure Java JDK: Window, Preferences, Java, Installed JREs, Add
>+ Standard VM, Directory and look for the path where Java JDK is installed (probably C:\Program Files\Java\jdk1.8.0_72), Finish
>+ Get the project: File, Import, Existing Maven Project, Look for the path where you download the source code.
>+ This will create a new project name "spring-4-eloy"
>+ Right click over the project, Run as, Maven build
>+ In "Goals" enter "clean package", Run
>+ Now, your project was build and a WAR file was created under target directory.
>+ Right click over the project, Run on Server, Select Apache Tomcat 8 (or 7) and Finish
>+ If you get a "publishing" error, just click over the project, press F5 and restart tomcat.

If everything went fine, you should be able to open (http://localhost:8080/spring-4-eloy/) in your browser.