'use strict';

App.factory('MovieService', ['$http', '$q', function($http, $q){

    return {

        fetchAllMovies: function() {
            return $http.get('movie/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching movies');
                        return $q.reject(errResponse);
                    }
                );
        },

        createMovie: function(movie){
            return $http.post('movie/', movie)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating movie');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateMovie: function(movie, id){
            return $http.put('movie/'+id, movie)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating movie');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteMovie: function(id){
            return $http.delete('movie/'+id)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting movie');
                        return $q.reject(errResponse);
                    }
                );
        }

    };

}]);