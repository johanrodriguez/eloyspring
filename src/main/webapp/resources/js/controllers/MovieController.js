'use strict';

App.controller('MovieController', ['$scope', 'MovieService', function($scope, MovieService) {
    var self = this;
    self.movie={id:null,moviename:'',address:'',email:''};
    self.movies=[];

    self.fetchAllMovies = function(){
        MovieService.fetchAllMovies()
            .then(
                function(d) {
                    self.movies = d;
                },
                function(errResponse){
                    console.error('Error while fetching Currencies');
                }
            );
    };

    self.createMovie = function(movie){
        MovieService.createMovie(movie)
            .then(
                self.fetchAllMovies,
                function(errResponse){
                    console.error('Error while creating Movie.');
                }
            );
    };

    self.updateMovie = function(movie, id){
        MovieService.updateMovie(movie, id)
            .then(
                self.fetchAllMovies,
                function(errResponse){
                    console.error('Error while updating Movie.');
                }
            );
    };

    self.deleteMovie = function(id){
        MovieService.deleteMovie(id)
            .then(
                self.fetchAllMovies,
                function(errResponse){
                    console.error('Error while deleting Movie.');
                }
            );
    };

    self.fetchAllMovies();

    self.submit = function() {
        if(self.movie.id===null){
            console.log('Saving New Movie', self.movie);
            self.createMovie(self.movie);
        }else{
            self.updateMovie(self.movie, self.movie.id);
            console.log('Movie updated with id ', self.movie.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.movies.length; i++){
            if(self.movies[i].id === id) {
                self.movie = angular.copy(self.movies[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.movie.id === id) {//clean form if the movie to be deleted is shown there.
            self.reset();
        }
        self.deleteMovie(id);
    };


    self.reset = function(){
        self.movie={id:null,name:'',year:'',artwork:''};
        $scope.moviesForm.$setPristine(); //reset Form
    };

}]);