<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html class="full" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Spring 4 Eloyalty</title>

    <c:url var="home" value="/" scope="request"/>

    <spring:url value="/resources/css/full.css" var="fullCss"/>
    <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${fullCss}" rel="stylesheet"/>
    <spring:url value="/resources/css/login.css" var="loginCss" />
    <link href="${loginCss}" rel="stylesheet" />
    <spring:url value="/resources/css/app.css" var="appCss" />
    <link href="${appCss}" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body ng-app="eloySpring" class="ng-cloak">
<header id="header">
    <tiles:insertAttribute name="header"/>
</header>

<section id="site-content">
    <tiles:insertAttribute name="body"/>
</section>

<footer id="footer">
    <tiles:insertAttribute name="footer"/>
</footer>

<spring:url value="/resources/js/lib/jquery.js" var="jqueryJs"/>
<script src="${jqueryJs}"></script>
<spring:url value="/resources/js/lib/bootstrap.js" var="bootstrapJs"/>
<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>

<script src="<c:url value='/resources/js/app.js' />"></script>
<script src="<c:url value='/resources/js/services/MovieService.js' />"></script>
<script src="<c:url value='/resources/js/controllers/MovieController.js' />"></script>
<script src="<c:url value='/resources/js/services/UserService.js' />"></script>
<script src="<c:url value='/resources/js/controllers/UserController.js' />"></script>

</body>
</html>