<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="col-xs-6 col-xs-offset-3" ng-controller="MovieController as ctrl" style="background-color: white">
    <h2><spring:message code="moviescrud"/></h2>
    <form ng-submit="ctrl.submit()" name="moviesForm">
        <label class="col-xs-12"><spring:message code="movie.name"/></label>
        <input type="text" ng-model="ctrl.movie.name" id="name" class="col-xs-12" required>
        <div class="has-error" ng-show="moviesForm.$dirty">
            <span ng-show="moviesForm.name.$error.required">This is a required field</span>
            <span ng-show="moviesForm.name.$error.minlength">Minimum length required is 3</span>
            <span ng-show="moviesForm.name.$invalid">This field is invalid </span>
        </div>

        <label class="col-xs-12"><spring:message code="movie.year"/></label>
        <input type="text" ng-model="ctrl.movie.year" id="year" class="col-xs-12" required>
        <div class="has-error" ng-show="moviesForm.$dirty">
            <span ng-show="moviesForm.year.$error.required">This is a required field</span>
            <span ng-show="moviesForm.year.$invalid">This field is invalid </span>
        </div>

        <label class="col-xs-12"><spring:message code="movie.artwork"/></label>
        <input type="url" ng-model="ctrl.movie.artwork" id="artwork" class="col-xs-12" required/>
        <div class="has-error" ng-show="moviesForm.$dirty">
            <span ng-show="moviesForm.artwork.$error.required">This is a required field</span>
            <span ng-show="moviesForm.artwork.$invalid">This field is invalid </span>
        </div>

        <div class="col-xs-12 form-actions">
            <input type="submit"  value="{{!ctrl.movie.id ? '<spring:message code="movie.add"/>' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="moviesForm.$invalid">
            <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="moviesForm.$pristine"><spring:message code="movie.resetform"/></button>
        </div>
        <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>
    <hr/>
    <div class="row custyle">
        <h2><spring:message code="movie.listofmovies"/></h2>
        <table class="table table-striped custab">
            <thead>
            <tr>
                <th>ID.</th>
                <th><spring:message code="movie.name"/></th>
                <th><spring:message code="movie.year"/></th>
                <th><spring:message code="movie.artwork"/></th>
                <th width="20%"></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="movie in ctrl.movies">
                <td><span ng-bind="movie.id"></span></td>
                <td><span ng-bind="movie.name"></span></td>
                <td><span ng-bind="movie.year"></span></td>
                <td><img src="{{movie.artwork}}" alt="movie.name" height="100" width="65"></span></td>
                <td>
                    <button type="button" ng-click="ctrl.edit(movie.id)" class="btn btn-success custom-width"><spring:message code="movies.edit"/></button>
                    <button type="button" ng-click="ctrl.remove(movie.id)" class="btn btn-danger custom-width"><spring:message code="movies.remove"/></button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>