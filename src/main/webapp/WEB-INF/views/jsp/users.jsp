<div class="col-xs-6 col-xs-offset-3" ng-controller="UserController as ctrl" style="background-color: white">
    <h2>Users CRUD</h2>
    <form ng-submit="ctrl.submit()" name="usersForm">
        <label class="col-xs-12">Name</label>
        <input type="text" ng-model="ctrl.user.username" id="username" class="col-xs-12" required>
        <div class="has-error" ng-show="usersForm.$dirty">
            <span ng-show="usersForm.username.$error.required">This is a required field</span>
            <span ng-show="usersForm.username.$invalid">This field is invalid </span>
        </div>

        <label class="col-xs-12">Password</label>
        <input type="text" ng-model="ctrl.user.password" id="password" class="col-xs-12" required>
        <div class="has-error" ng-show="usersForm.$dirty">
            <span ng-show="usersForm.username.$error.required">This is a required field</span>
            <span ng-show="usersForm.username.$invalid">This field is invalid </span>
        </div>

        <label class="col-xs-12">Email</label>
        <input type="email" ng-model="ctrl.user.email" id="email" class="col-xs-12" required>
        <div class="has-error" ng-show="usersForm.$dirty">
            <span ng-show="usersForm.email.$error.required">This is a required field</span>
            <span ng-show="usersForm.email.$invalid">This field is invalid </span>
        </div>

        <label class="col-xs-12">Roles</label>
        <input type="text" ng-model="ctrl.user.roles" id="roles" class="col-xs-12" required>
        <div class="has-error" ng-show="usersForm.$dirty">
            <span ng-show="usersForm.roles.$error.required">This is a required field</span>
            <span ng-show="usersForm.roles.$invalid">This field is invalid </span>
        </div>

        <div class="col-xs-12 form-actions">
            <input type="submit"  value="{{!ctrl.user.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="usersForm.$invalid">
            <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="usersForm.$pristine">Reset Form</button>
        </div>
        <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>
    <hr/>
    <div class="row custyle">
        <h2>List of users</h2>
        <table class="table table-striped custab">
            <thead>
            <tr>
                <th>ID.</th>
                <th>Username</th>
                <th>Email</th>
                <th>Roles</th>
                <th width="20%"></th>
            </tr>
            </thead>
            <tbody>
            <tr ng-repeat="user in ctrl.users">
                <td><span ng-bind="user.id"></span></td>
                <td><span ng-bind="user.username"></span></td>
                <td><span ng-bind="user.email"></span></td>
                <td><span ng-bind="user.roles"></span></td>
                <td>
                    <button type="button" ng-click="ctrl.edit(user.id)" class="btn btn-success custom-width">Edit</button>
                    <button type="button" ng-click="ctrl.remove(user.id)" class="btn btn-danger custom-width">Remove</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>