package com.eloysample.web.service;

import com.eloysample.web.model.Movie;

import java.util.List;

/**
 * Created by eloy03780 on 7/5/2016.
 */
public interface MovieService {
    Movie findById(String id);

    Movie findByName(String name);

    void saveMovie(Movie movie);

    void updateMovie(Movie movie);

    void deleteMovieById(String id);

    List<Movie> findAllMovies();

    public boolean isMovieExist(Movie movie);
}
