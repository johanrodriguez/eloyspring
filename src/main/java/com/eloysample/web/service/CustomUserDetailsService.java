package com.eloysample.web.service;

import com.eloysample.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eloy03780 on 7/6/2016.
 */
@Service("userDetailService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    MongoOperations mongoOperation;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = getUserDetail(username);
        org.springframework.security.core.userdetails.User userDetail = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true, getAuthorities(user.getRoles()));
        return userDetail;
    }

    public List<GrantedAuthority> getAuthorities(String roles) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();

        for (String role:roles.split(",")){
            authList.add(new SimpleGrantedAuthority(role));
        }
        /*if (role.intValue() == 1) {
            authList.add(new SimpleGrantedAuthority("ROLE_USER"));
            authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        } else if (role.intValue() == 2) {
            authList.add(new SimpleGrantedAuthority("ROLE_USER"));
        }*/
        return authList;
    }

    public User getUserDetail(String username) {
        //Let's create the first users
        List<User> users = mongoOperation.findAll(User.class);
        if(users.isEmpty()){
            User user = new User("eloyadmin", "123456");
            user.setRoles("ROLE_USER,ROLE_ADMIN");
            mongoOperation.save(user);
            user = new User("eloyuser", "123456");
            user.setRoles("ROLE_USER");
            mongoOperation.save(user);
        }


        User user = mongoOperation.findOne(
                new Query(Criteria.where("username").is(username)),
                User.class);
        System.out.println(user.toString());
        return user;
    }
}
