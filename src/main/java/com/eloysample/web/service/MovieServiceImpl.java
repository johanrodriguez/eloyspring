package com.eloysample.web.service;

import com.eloysample.web.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by eloy03780 on 7/5/2016.
 */
@Service("movieService")
@Transactional
public class MovieServiceImpl implements MovieService{
    @Autowired
    MongoOperations mongoOperation;

    @Override
    public Movie findById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        return mongoOperation.findOne(query, Movie.class);
    }

    @Override
    public Movie findByName(String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where("name").is(name));
        return mongoOperation.findOne(query, Movie.class);
    }

    @Override
    public void saveMovie(Movie movie) {
        mongoOperation.save(movie);
    }


    @Override
    public void updateMovie(Movie movie) {
        mongoOperation.save(movie);
    }

    @Override
    public void deleteMovieById(String id) {
        Movie movie = findById(id);
        mongoOperation.remove(movie);
    }

    @Override
    public List<Movie> findAllMovies() {
        return mongoOperation.findAll(Movie.class);
    }

    @Override
    public boolean isMovieExist(Movie movie) {
        return findByName(movie.getName())!=null;
    }
}
