package com.eloysample.web.service;

import com.eloysample.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by eloy03780 on 7/6/2016.
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    MongoOperations mongoOperation;

    @Override
    public User findById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        return mongoOperation.findOne(query, User.class);
    }

    @Override
    public User findByUsername(String username) {
        Query query = new Query();
        query.addCriteria(Criteria.where("username").is(username));
        return mongoOperation.findOne(query, User.class);
    }

    @Override
    public void saveUser(User user) {
        mongoOperation.save(user);
    }


    @Override
    public void updateUser(User user) {
        mongoOperation.save(user);
    }

    @Override
    public void deleteUserById(String id) {
        User user = findById(id);
        mongoOperation.remove(user);
    }

    @Override
    public List<User> findAllUsers() {
        return mongoOperation.findAll(User.class);
    }

    @Override
    public boolean isUserExist(User user) {
        return findByUsername(user.getUsername())!=null;
    }
}
