package com.eloysample.web.service;

import com.eloysample.web.model.User;

import java.util.List;

/**
 * Created by eloy03780 on 7/6/2016.
 */
public interface UserService {
    User findById(String id);

    User findByUsername(String username);

    void saveUser(User User);

    void updateUser(User User);

    void deleteUserById(String id);

    List<User> findAllUsers();

    public boolean isUserExist(User user);
}
