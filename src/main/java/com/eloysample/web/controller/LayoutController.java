package com.eloysample.web.controller;

import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LayoutController {

    @RequestMapping(value =  "/", method = RequestMethod.GET)
    public ModelAndView indexPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is welcome page!");
        model.setViewName("index");
        return model;
    }

    @RequestMapping(value =  "/welcome**", method = RequestMethod.GET)
    public ModelAndView welcomePage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is welcome page!");
        model.setViewName("welcome");
        return model;
    }

    @RequestMapping(value = "/admin**", method = RequestMethod.GET)
    public ModelAndView adminPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Hi... I'm Admin");
        model.addObject("message", "This is protected page - Admin Page!");
        model.setViewName("admin");
        return model;
    }

    @RequestMapping(value = "/dba**", method = RequestMethod.GET)
    public ModelAndView dbaPage(Device device) {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Hi... I'm DBA");
        if(device.isMobile()){
            model.addObject("message", "This is protected page IN A MOBILE DEVICE - Database Page!");
        } else {
            model.addObject("message", "This is protected page - Database Page!");
        }
        model.setViewName("admin");
        return model;
    }

    //Spring Security see this :
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

        return model;
    }

    @RequestMapping(value =  "/movies", method = RequestMethod.GET)
    public ModelAndView moviesPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("movies");
        return model;
    }

    @RequestMapping(value =  "/users", method = RequestMethod.GET)
    public ModelAndView usersPage() {
        ModelAndView model = new ModelAndView();
        model.setViewName("users");
        return model;
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public ModelAndView accessDeniedPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("title", "Hi... Access Denied");
        model.addObject("message", "This is protected page and you have no access to it!");
        model.setViewName("403");
        return model;
    }

}
