package com.eloysample.web.controller;

import com.eloysample.web.model.Movie;
import com.eloysample.web.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

/**
 * Created by eloy03780 on 7/5/2016.
 */
@RestController
public class MovieController {

    @Autowired
    MovieService movieService;  //Service which will do all data retrieval/manipulation work

    //-------------------Retrieve All Movies--------------------------------------------------------

    @RequestMapping(value = "/movie/", method = RequestMethod.GET)
    public ResponseEntity<List<Movie>> listAllMovies() {
        List<Movie> movies = movieService.findAllMovies();
        if(movies.isEmpty()){
            return new ResponseEntity<List<Movie>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Movie>>(movies, HttpStatus.OK);
    }



    //-------------------Retrieve Single Movie--------------------------------------------------------

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Movie> getMovie(@PathVariable("id") String id) {
        System.out.println("Fetching Movie with id " + id);
        Movie movie = movieService.findById(id);
        if (movie == null) {
            System.out.println("Movie with id " + id + " not found");
            return new ResponseEntity<Movie>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Movie>(movie, HttpStatus.OK);
    }



    //-------------------Create a Movie--------------------------------------------------------

    @RequestMapping(value = "/movie/", method = RequestMethod.POST)
    public ResponseEntity<Void> createMovie(@RequestBody Movie movie,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Movie " + movie.getName());

        if (movieService.isMovieExist(movie)) {
            System.out.println("A Movie with name " + movie.getName() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        movieService.saveMovie(movie);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/movie/{id}").buildAndExpand(movie.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }



    //------------------- Update a Movie --------------------------------------------------------

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Movie> updateMovie(@PathVariable("id") String id, @RequestBody Movie movie) {
        System.out.println("Updating Movie " + id);

        Movie currentMovie = movieService.findById(id);

        if (currentMovie==null) {
            System.out.println("Movie with id " + id + " not found");
            return new ResponseEntity<Movie>(HttpStatus.NOT_FOUND);
        }

        currentMovie.setName(movie.getName());
        currentMovie.setYear(movie.getYear());
        currentMovie.setArtwork(movie.getArtwork());

        movieService.updateMovie(currentMovie);
        return new ResponseEntity<Movie>(currentMovie, HttpStatus.OK);
    }



    //------------------- Delete a Movie --------------------------------------------------------

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Movie> deleteMovie(@PathVariable("id") String id) {
        System.out.println("Fetching & Deleting Movie with id " + id);

        Movie movie = movieService.findById(id);
        if (movie == null) {
            System.out.println("Unable to delete. Movie with id " + id + " not found");
            return new ResponseEntity<Movie>(HttpStatus.NOT_FOUND);
        }

        movieService.deleteMovieById(id);
        return new ResponseEntity<Movie>(HttpStatus.NO_CONTENT);
    }



    //------------------- Delete All Movies --------------------------------------------------------

    /*@RequestMapping(value = "/movie/", method = RequestMethod.DELETE)
    public ResponseEntity<Movie> deleteAllMovies() {
        System.out.println("Deleting All Movies");

        movieService.deleteAllMovies();
        return new ResponseEntity<Movie>(HttpStatus.NO_CONTENT);
    }*/
}
